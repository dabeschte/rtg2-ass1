extern const char fs_lambert[] = 
  "#version 440\n"
  "#line 0 0\n"
  "\n"
  "\n"
  "\n"  "#line 0 1\n"
  "\n"
  "\n"
  "#ifndef INCLUDED_CAMERA\n"
  "#define INCLUDED_CAMERA\n"
  "\n"
  "struct Camera\n"
  "{\n"
  "  mat4x3 V;\n"
  "  mat4x3 V_inv;\n"
  "  mat4x4 P;\n"
  "  mat4x4 P_inv;\n"
  "  mat4x4 PV;\n"
  "  mat4x4 PV_inv;\n"
  "  vec3 position;\n"
  "};\n"
  "\n"
  "layout(std140, row_major, binding = 0) uniform CameraParameters\n"
  "{\n"
  "  Camera camera;\n"
  "};\n"
  "\n"
  "#endif  \n"  "\n"
  "\n"
  "in vec3 p;\n"
  "in vec3 normal;\n"
  "\n"
  "layout(location = 0) out vec4 color;\n"
  "\n"
  "void main()\n"
  "{\n"
  "	vec3 n = normalize(normal);\n"
  "	vec3 v = normalize(camera.position - p);\n"
  "	float l = max(dot(n, v), 0.0f);\n"
  "	color = vec4(l, l, l, 1.0f);\n"
  "}\n"
  "\n";
