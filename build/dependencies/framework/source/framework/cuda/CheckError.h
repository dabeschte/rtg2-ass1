#ifndef CHECKERROR_H
#define CHECKERROR_H

#pragma once

#include <iostream>
#include <cassert>
#include <cuda_runtime.h>

#define checkCudaError(x) _checkCudaError(x, __FILE__, __LINE__)

inline void _checkCudaError(cudaError_t error, const char* file, const int line)
{
  if (error != cudaSuccess)
  {
    std::cout << "[" << file << ":" << line << "] got CUDA error " << error << ": " << cudaGetErrorString(error)
              << std::endl;
    assert(false);
  }
}
#endif /*CHECKERROR_H*/
