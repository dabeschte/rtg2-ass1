

#ifndef INCLUDED_PERSPECTIVE_CAMERA
#define INCLUDED_PERSPECTIVE_CAMERA

#pragma once

#include "framework/math/vector.h"

#include "Camera.h"

class PerspectiveCamera : public virtual Camera
{
private:
  float fov;
  float nearz;
  float farz;
  float d;

public:
  PerspectiveCamera(float fov = 60.0f * math::constants<float>::pi() / 180.0f, float nearz = 0.1f, float farz = 100.0f);

  void writeUniformBuffer(UniformBuffer* buffer, const Navigator& navigator, float aspect) const;
  void writeUniformBuffer(UniformBuffer* buffer, const Navigator& navigator, float aspect, float d) const;
  void writeUniformBufferL(UniformBuffer* buffer, const Navigator& navigator, float aspect) const;
  void writeUniformBufferR(UniformBuffer* buffer, const Navigator& navigator, float aspect) const;
};

#endif // INCLUDED_PERSPECTIVE_CAMERA
