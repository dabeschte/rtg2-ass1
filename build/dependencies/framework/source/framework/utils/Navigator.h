

#ifndef INCLUDED_NAVIGATOR
#define INCLUDED_NAVIGATOR

#pragma once

#include "framework/math/matrix.h"

#include "interface.h"

class INTERFACE Navigator
{
protected:
  Navigator() = default;
  Navigator(const Navigator&) = default;
  Navigator& operator=(const Navigator&) = default;
  ~Navigator() = default;

public:
  virtual void reset() = 0;
  virtual void writeWorldToLocalTransform(math::affine_float4x4* M) const = 0;
  virtual void writeLocalToWorldTransform(math::affine_float4x4* M) const = 0;
  virtual void writePosition(math::float3* p) const = 0;
};

#endif // INCLUDED_NAVIGATOR
