


#version 440

#include <framework/utils/GLSL/camera>

in vec3 p;
in vec3 normal;

layout(location = 0) out vec4 color;

void main()
{
	vec3 n = normalize(normal);
	vec3 v = normalize(camera.position - p);
	float l = max(dot(n, v), 0.0f);
	color = vec4(l, l, l, 1.0f);
}
