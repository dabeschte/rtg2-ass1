


#version 440

#include <framework/utils/GLSL/camera>

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;

out vec3 p;
out vec3 normal;

void main()
{
	gl_Position = camera.PV * vec4(v_position, 1.0f);
	p = v_position;
	normal = v_normal;
}
