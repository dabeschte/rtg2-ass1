#pragma once

#include <unordered_map>
#include <stdint.h>
#include "../math/vector.h"

class IcoSphere
{

  using Triangle = math::uint3;

  uint32_t index;
  std::unordered_map<uint64_t, uint32_t> middlePointIndexCache;

  std::vector<math::float4> vertices;
  std::vector<uint32_t> indices;

  // add vertex to mesh, fix position to be on unit sphere, return index
  uint32_t addVertex(const math::float3 & p)
  {
    vertices.push_back(math::float4(normalize(p),1));
    return index++;
  }

  uint32_t addVertex(const math::float4 & p)
  {
    vertices.push_back(math::float4(normalize(p.xyz()), 1));
    return index++;
  }

  void addFace(uint32_t i0, uint32_t i1, uint32_t i2)
  {
    indices.push_back(i0);
    indices.push_back(i1);
    indices.push_back(i2);
  }

  // return index of point in the middle of p1 and p2
  int getMiddlePoint(uint32_t p1, uint32_t p2)
  {
    // first check if we have it already
    bool firstIsSmaller = p1 < p2;
    uint64_t smallerIndex = firstIsSmaller ? p1 : p2;
    uint64_t greaterIndex = firstIsSmaller ? p2 : p1;
    uint64_t key = (smallerIndex << 32) | greaterIndex;

    auto found = middlePointIndexCache.find(key);
    if (found != middlePointIndexCache.end())
      return found->second;

    // not in cache, calculate it
    math::float4 middle = 0.5f * (vertices[p1] + vertices[p2]);
    uint32_t i = addVertex(middle);

    middlePointIndexCache.insert(std::make_pair(key, i));
    return i;
  }

public:

  IcoSphere(uint32_t recursionLevel) : index(0)
  {

    // create 12 vertices of a icosahedron
    float t = (1.0f + sqrt(5.0f)) / 2.0f;
    addVertex(math::float3(-1, t, 0));
    addVertex(math::float3(-1, t, 0));
    addVertex(math::float3(1, t, 0));
    addVertex(math::float3(-1, -t, 0));
    addVertex(math::float3(1, -t, 0));

    addVertex(math::float3(0, -1, t));
    addVertex(math::float3(0, 1, t));
    addVertex(math::float3(0, -1, -t));
    addVertex(math::float3(0, 1, -t));

    addVertex(math::float3(t, 0, -1));
    addVertex(math::float3(t, 0, 1));
    addVertex(math::float3(-t, 0, -1));
    addVertex(math::float3(-t, 0, 1));


    // 5 faces around point 0
    addFace(0, 11, 5);
    addFace(0, 5, 1);
    addFace(0, 1, 7);
    addFace(0, 7, 10);
    addFace(0, 10, 11);

    // 5 adjacent faces 
    addFace(1, 5, 9);
    addFace(5, 11, 4);
    addFace(11, 10, 2);
    addFace(10, 7, 6);
    addFace(7, 1, 8);

    // 5 faces around point 3
    addFace(3, 9, 4);
    addFace(3, 4, 2);
    addFace(3, 2, 6);
    addFace(3, 6, 8);
    addFace(3, 8, 9);

    // 5 adjacent faces 
    addFace(4, 9, 5);
    addFace(2, 4, 11);
    addFace(6, 2, 10);
    addFace(8, 6, 7);
    addFace(9, 8, 1);


    // refine triangles
    for (uint32_t i = 0; i < recursionLevel; i++)
    {
      decltype(indices) old_indices;
      old_indices.swap(indices);
      for (size_t j = 0; j < old_indices.size(); j += 3)
      {
        uint32_t v1 = old_indices[j],
          v2 = old_indices[j + 1],
          v3 = old_indices[j + 2];

        // replace triangle by 4 triangles
        uint32_t a = getMiddlePoint(v1, v2);
        uint32_t b = getMiddlePoint(v2, v3);
        uint32_t c = getMiddlePoint(v3, v1);

        addFace(v1, a, c);
        addFace(v2, b, a);
        addFace(v3, c, b);
        addFace(a, b, c);
      }
    }
  }

  size_t numVertices() const { return vertices.size(); }
  size_t numTriangles() const { return indices.size() / 3; }
  const float* getVertices() const { return &vertices[0].x; }
  const uint32_t* getInidices() const { return &indices[0]; }
};