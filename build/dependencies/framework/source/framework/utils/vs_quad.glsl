


#version 440

uniform vec4 pos;

out vec2 t;

void main()
{
	vec2 p = vec2((gl_VertexID & 0x1), (gl_VertexID & 0x2) * 0.5f);
	gl_Position = vec4(pos.zw * 0.5f * (p * 2.0f - 1.0f) + pos.xy, 0.0f, 1.0f);
	t = p;
}
