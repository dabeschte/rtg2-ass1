

#include "PerspectiveCamera.h"

namespace
{
math::float4x4 proj(float fov, float aspect, float nearz, float farz)
{
  const float s_y = 1.0f / math::tan(fov * 0.5f);
  const float s_x = s_y / aspect;
  const float z_1 = -(farz + nearz) / (farz - nearz);
  const float z_2 = -2.0f * nearz * farz / (farz - nearz);

  return math::float4x4(s_x, 0.0f, 0.0f, 0.0f, 0.0f, s_y, 0.0f, 0.0f, 0.0f, 0.0f, z_1, z_2, 0.0f, 0.0f, -1.0f, 0.0f);
}

math::float4x4 proj(float fov, float aspect, float nearz, float farz, float d)
{
  const float s_y = 1.0f / math::tan(fov * 0.5f);
  const float s_x = s_y / aspect;
  const float z_1 = -(farz + nearz) / (farz - nearz);
  const float z_2 = -2.0f * nearz * farz / (farz - nearz);
  const float m_2 = -s_x * d * 0.5f;
  const float m_1 = m_2 / nearz;

  return math::float4x4(s_x, 0.0f, m_1, m_2, 0.0f, s_y, 0.0f, 0.0f, 0.0f, 0.0f, z_1, z_2, 0.0f, 0.0f, -1.0f, 0.0f);
}
}

PerspectiveCamera::PerspectiveCamera(float fov, float nearz, float farz)
  : fov(fov)
  , nearz(nearz)
  , farz(farz)
{
}

void PerspectiveCamera::writeUniformBuffer(UniformBuffer* buffer, const Navigator& navigator, float aspect) const
{
  navigator.writeWorldToLocalTransform(&buffer->V);
  navigator.writeLocalToWorldTransform(&buffer->V_inv);
  buffer->P = proj(fov, aspect, nearz, farz);
  buffer->P_inv = inverse(buffer->P);
  buffer->PV = buffer->P * buffer->V;
  buffer->PV_inv = buffer->V_inv * buffer->P_inv;
  navigator.writePosition(&buffer->position);
}

void PerspectiveCamera::writeUniformBuffer(UniformBuffer* buffer, const Navigator& navigator, float aspect,
                                           float d) const
{
  navigator.writeWorldToLocalTransform(&buffer->V);
  navigator.writeLocalToWorldTransform(&buffer->V_inv);
  buffer->P = proj(fov, aspect, nearz, farz, d);
  buffer->P_inv = inverse(buffer->P);
  buffer->PV = buffer->P * buffer->V;
  buffer->PV_inv = buffer->V_inv * buffer->P_inv;
  navigator.writePosition(&buffer->position);
}

void PerspectiveCamera::writeUniformBufferL(UniformBuffer* buffer, const Navigator& navigator, float aspect) const
{
  writeUniformBuffer(buffer, navigator, aspect, -nearz * 0.05f);
}

void PerspectiveCamera::writeUniformBufferR(UniformBuffer* buffer, const Navigator& navigator, float aspect) const
{
  writeUniformBuffer(buffer, navigator, aspect, nearz * 0.05f);
}
