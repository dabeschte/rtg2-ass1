

#ifndef INCLUDED_CAMERA
#define INCLUDED_CAMERA

#pragma once

#include "framework/math/vector.h"
#include "framework/math/matrix.h"

#include "interface.h"
#include "Navigator.h"

class INTERFACE Camera
{
protected:
  Camera() = default;
  Camera(const Camera&) = default;
  Camera& operator=(const Camera&) = default;
  ~Camera() = default;

public:
  struct UniformBuffer
  {
    math::affine_float4x4 V;
    math::affine_float4x4 V_inv;
    math::float4x4 P;
    math::float4x4 P_inv;
    math::float4x4 PV;
    math::float4x4 PV_inv;
    math::float3 position;
  };

  virtual void writeUniformBuffer(UniformBuffer* buffer, const Navigator& navigator, float aspect) const = 0;
  virtual void writeUniformBuffer(UniformBuffer* buffer, const Navigator& navigator, float aspect, float d) const = 0;
  virtual void writeUniformBufferL(UniformBuffer* buffer, const Navigator& navigator, float aspect) const = 0;
  virtual void writeUniformBufferR(UniformBuffer* buffer, const Navigator& navigator, float aspect) const = 0;
};

#endif // INCLUDED_CAMERA
