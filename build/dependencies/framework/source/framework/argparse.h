#pragma once

#include <string>
#include <cstring>
#include <stdexcept>

namespace argparse
{
struct usage_error : std::runtime_error
{
  explicit usage_error(const std::string& msg)
    : runtime_error(msg)
  {
  }
};

template <std::size_t N>
std::size_t strlen(const char (&)[N])
{
  return N - 1;
}

char** parseArgument(char** argv, size_t token_offset, int& v)
{
  const char* startptr = *argv + token_offset;
  char* argend = *argv + std::strlen(*argv);

  if (startptr == argend)
  {
    startptr = *++argv;
    argend = *argv + std::strlen(*argv);
  }

  char* endptr = nullptr;
  v = std::strtol(startptr, &endptr, 10);

  if (endptr < argend)
    throw usage_error("expected integer argument");

  return argv;
}

char** parseArgument(char** argv, size_t token_offset, float& v)
{
  const char* startptr = *argv + token_offset;
  char* argend = *argv + std::strlen(*argv);

  if (startptr == argend)
  {
    startptr = *++argv;
    argend = *argv + std::strlen(*argv);
  }

  char* endptr = nullptr;
  v = std::strtof(startptr, &endptr);

  if (endptr < argend)
    throw usage_error("expected float argument");

  return argv;
}

template <int S, typename T>
bool checkArgument(const char (&token)[S], char**& a, T& v)
{
  if (std::strncmp(token, *a, strlen(token)) == 0)
  {
    a = parseArgument(a, strlen(token), v);
    return true;
  }
  return false;
}
template <int S>
bool checkArgument(const char (&token)[S], char**& a, bool& set)
{
  if (std::strncmp(token, *a, strlen(token)) == 0)
  {
    set = true;
    return true;
  }
  return false;
}
}
