cmake_minimum_required(VERSION 2.8)

add_subdirectory("${DEPENDENCIES_DIR}/GL_core_tools/build/cmake" GL_core_tools)
include("${DEPENDENCIES_DIR}/GLSL_build_tools/build/cmake/add_glsl_sources.cmake")

project(utils)

set(SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../../source/framework/utils")

file(GLOB utils_SOURCES "${SOURCE_DIR}/*.h" "${SOURCE_DIR}/*.cpp")
file(GLOB glsl_SOURCES "${SOURCE_DIR}/*.glsl")

include_directories(${Framework_INCLUDE_DIRS} ${GL_core_tools_INCLUDE_DIRS})

add_library(utils STATIC ${utils_SOURCES})

glsl_add_include_directories("${glsl_SOURCES}" "${SOURCE_DIR}/../..")
add_glsl_sources(common_shaders "${glsl_SOURCES}")

set(utils_INCLUDE_DIR "${SOURCE_DIR}" PARENT_SCOPE)
