# Framework for RTG2

This contains all common code for the RTG2 assignments. This repository is
checked out by the pull script within the task repositories.

This is not intended to build by itself, rather it is designed to be included
by the build process of tasks.
