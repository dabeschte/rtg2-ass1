#ifndef RENDERER_H_OIBQH5XL
#define RENDERER_H_OIBQH5XL

#pragma once

#include "GL/buffer.h"
#include "GL/gl.h"
#include "GL/platform/Context.h"
#include "GL/platform/DefaultDisplayHandler.h"
#include "GL/platform/Window.h"
#include "GL/shader.h"
#include "GL/texture.h"
#include "GL/vertex_array.h"

#include "framework/BasicRenderer.h"
#include "framework/utils/Camera.h"
#include "framework/utils/OrbitalNavigator.h"

#include <array>
#include <chrono>
#include <cstdint>
#include <fstream>
#include <vector>

#include "ClothSimulation.h"

class Renderer : public GL::platform::Renderer,
                 public GL::platform::DisplayHandler
{
public:
  Renderer(const Renderer&) = delete;
  Renderer& operator=(const Renderer&) = delete;

  Renderer(GL::platform::Window& window, Camera& camera,
           OrbitalNavigator& navigator, int opengl_version_major,
           int opengl_version_minor, int grid_size, float cloth_dim,
           float cloth_height, float cloth_mass, int fixUpIts, float fixUpStep);
  ~Renderer();

  virtual void render() override;

  virtual void move(int x, int y) override;
  virtual void resize(int width, int height) override;
  virtual void close() override;
  virtual void destroy() override;

  void initGrid();
  void freeze();
  void setBenchmark(bool benchmark);
  void adjustSimulationSpeed(double factor);
  void switchRendermode();
  void switchScene(int i);
  void toggleWind();

protected:
  GL::platform::Window& window;
  Camera& camera;
  OrbitalNavigator& navigator;

  GL::platform::Context context;
  GL::platform::context_scope<GL::platform::Window> context_scope;

  int viewport_width{0};
  int viewport_height{0};

  GL::VertexShader matrix_vs;
  GL::FragmentShader phong_fs;
  GL::Program phong_prog;

  GL::VertexShader clothes_vs;
  GL::GeometryShader clothes_gs;
  GL::FragmentShader clothes_fs;
  GL::Program clothes_prog;

  GL::Buffer camera_uniform_buffer;

  GL::Buffer clothes_vertex_buffer[4];
  GL::Buffer clothes_vertex_color;
  GL::VertexArray clothes_vao;

  GL::Buffer sphere_vertex_buffer;
  GL::Buffer sphere_index_buffer;
  uint32_t sphere_triangles;
  GLint spherePos, sphereRadius;
  GL::VertexArray sphere_vao;

  std::chrono::high_resolution_clock::time_point refTime;
  float simulationTime;
  float renderTime;
  int benchmarkScreenshotInterval;
  int benchmarkSimulation;
  std::fstream benchmarkTimes;

  int gridSize{256};
  int renderMode{0};
  int frame_counter{0};
  int currentVertices{0};

  float cloth_size_x{2.f};
  float cloth_size_z{2.f};
  float cloth_height{1.5f};
  float cloth_mass{0.05f};
  int fixupIterations;
  float fixupPercent;

  std::vector<math::float4> obstacles;

  enum class WindType
  {
    NoWind,
    LowSide,
    StrongSide,
    LowBottom,
    StrongBottom,
    MovingSide
  };

  WindType currentWind{WindType::NoWind};
  int currentScene{0};

  float wind_x{0}, wind_y{0}, wind_z{0};

  const char* deftitle{"RTG Task 1: Cloth Simulation"};
  const float simulationStep{0.001f};
  double lastDisplayUpdate;
  double lastSimulation{-1.0};
  double simulationSpeed{1.0};
  int maxSimulationsPerFrame{50};
  bool benchmark{false};
  bool frozen{false};

  cudaEvent_t start, stop;

  ///////////////////////////////////////////////////////////////////////////////
  void createGrid();

  ///////////////////////////////////////////////////////////////////////////////

  void drawObstacles();
  void drawClothes();
  void updateSimulation(double now);
  void swapBuffers();

  ClothSimulation sim;
};

#endif /* end of include guard: RENDERER_H_OIBQH5XL */
