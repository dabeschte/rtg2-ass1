

#include "Renderer.h"

#include "GL/error.h"
#include "GL/platform/Application.h"
#include "GL/platform/Window.h"

#include "framework/math/vector.h"

#include "framework/utils/Sphere.h"

#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
#include <vector>

///////////////////////////////////////////////////////////////////////////////

// generated from the shaders by the build system
extern const char clothes_vs[];
extern const char clothes_gs[];
extern const char view_vs[];
extern const char phong_fs[];

///////////////////////////////////////////////////////////////////////////////

Renderer::Renderer(GL::platform::Window& window, Camera& camera,
                   OrbitalNavigator& navigator, int opengl_version_major,
                   int opengl_version_minor, int grid_size, float cloth_dim,
                   float cloth_height, float cloth_mass, int fixUpIts,
                   float fixUpStep)
  : GL::platform::Renderer()
  , window(window)
  , camera(camera)
  , navigator(navigator)
  , context(
        window.createContext(opengl_version_major, opengl_version_minor, true))
  , context_scope(context, window)
  , gridSize(grid_size)
  , cloth_size_x(cloth_dim)
  , cloth_size_z(cloth_dim)
  , cloth_height(cloth_height)
  , cloth_mass(cloth_mass)
  , fixupIterations(fixUpIts)
  , fixupPercent(fixUpStep)
  , sim(grid_size, cloth_size_x, cloth_size_z, cloth_mass, 0.995f, -9.81f,
        fixUpIts, fixUpStep, nullptr, 0)
{
  // init openGL
  glClearColor(0.1f, 0.3f, 1.0f, 1.0f);
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  GL::checkError();

  clothes_vs = GL::compileVertexShader(::clothes_vs);
  clothes_gs = GL::compileGeometryShader(::clothes_gs);
  phong_fs = GL::compileFragmentShader(::phong_fs);
  matrix_vs = GL::compileVertexShader(view_vs);

  glAttachShader(clothes_prog, clothes_vs);
  glAttachShader(clothes_prog, clothes_gs);
  glAttachShader(clothes_prog, phong_fs);
  GL::linkProgram(clothes_prog);

  glAttachShader(phong_prog, matrix_vs);
  glAttachShader(phong_prog, phong_fs);
  GL::linkProgram(phong_prog);

  // init the camera
  glBindBuffer(GL_UNIFORM_BUFFER, camera_uniform_buffer);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(Camera::UniformBuffer), nullptr,
               GL_STATIC_DRAW);

  GL::checkError();

  glBindVertexArray(clothes_vao);
  glUseProgram(clothes_prog);
  GLuint clothes_camera_parameters =
      glGetUniformBlockIndex(clothes_prog, "CameraParameters");
  glUniformBlockBinding(clothes_prog, clothes_camera_parameters, 0);

  glBindBuffer(GL_UNIFORM_BUFFER, camera_uniform_buffer);

  GL::checkError();

  // prepare the buffers for the spheres in openGL
  IcoSphere sphere(6);

  glBindVertexArray(sphere_vao);
  glUseProgram(phong_prog);
  glBindBuffer(GL_ARRAY_BUFFER, sphere_vertex_buffer);
  glBindVertexBuffer(0U, sphere_vertex_buffer, 0U, sizeof(float) * 4);
  glEnableVertexAttribArray(0U);

  glVertexAttribFormat(0U, 4, GL_FLOAT, GL_FALSE, 0U);
  glVertexAttribBinding(0U, 0U);

  glBindBuffer(GL_ARRAY_BUFFER, sphere_vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, sphere.numVertices() * 4 * 4,
               sphere.getVertices(), GL_STATIC_DRAW);

  sphere_triangles = sphere.numTriangles();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphere_index_buffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sphere.numTriangles() * 4 * 3,
               sphere.getInidices(), GL_STATIC_DRAW);

  spherePos = glGetUniformLocation(phong_prog, "pos");
  sphereRadius = glGetUniformLocation(phong_prog, "radius");

  glBindVertexArray(0);

  GL::checkError();

  createGrid();
  GL::checkError();

  glDisable(GL_CULL_FACE);

  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  window.attach(this);
}

Renderer::~Renderer()
{
  cudaEventDestroy(start);
  cudaEventDestroy(stop);
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::setBenchmark(bool benchmark)
{
  this->benchmark = benchmark;
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::resize(int width, int height)
{
  if (width == viewport_width && height == viewport_height)
  {
    return;
  }
  viewport_width = width;
  viewport_height = height;
  std::cout << "Resizing to " << width << "x" << height << std::endl;
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::freeze()
{
  frozen = !frozen;
  if (frozen)
  {
    std::cout << "Freezing simulation" << std::endl;
  }
  else
  {
    std::cout << "Resuming simulation" << std::endl;
  }
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::swapBuffers()
{
  context_scope.swapBuffers();
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::drawObstacles()
{
  glUseProgram(phong_prog);

  glBindVertexArray(sphere_vao);

  for (auto& info : obstacles)
  {
    glUniform3f(spherePos, info.x, info.y, info.z);
    glUniform1f(sphereRadius, 0.96f * info.w);
    glDrawElements(GL_TRIANGLES, 3 * sphere_triangles, GL_UNSIGNED_INT,
                   nullptr);
  }

  GL::checkError();
}

void Renderer::drawClothes()
{
  glUseProgram(clothes_prog);

  if (renderMode)
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glBindVertexArray(clothes_vao);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2,
                   clothes_vertex_buffer[currentVertices]);
  glDrawArrays(GL_POINTS, 0, (gridSize - 1) * (gridSize - 1));
  GL::checkError();

  if (renderMode)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::createGrid()
{
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, clothes_vertex_buffer[0]);
  glBufferData(GL_SHADER_STORAGE_BUFFER, gridSize * gridSize * 4 * 4, nullptr,
               GL_STATIC_DRAW);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, clothes_vertex_buffer[1]);
  glBufferData(GL_SHADER_STORAGE_BUFFER, gridSize * gridSize * 4 * 4, nullptr,
               GL_STATIC_DRAW);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, clothes_vertex_buffer[2]);
  glBufferData(GL_SHADER_STORAGE_BUFFER, gridSize * gridSize * 4 * 4, nullptr,
               GL_STATIC_DRAW);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, clothes_vertex_buffer[3]);
  glBufferData(GL_SHADER_STORAGE_BUFFER, gridSize * gridSize * 4 * 4, nullptr,
               GL_STATIC_DRAW);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, clothes_vertex_color);
  glBufferData(GL_SHADER_STORAGE_BUFFER, gridSize * gridSize * 4 * 4, nullptr,
               GL_STATIC_DRAW);

  GL::checkError();

  glBindVertexArray(clothes_vao);
  glUseProgram(clothes_prog);
  GL::checkError();
  GLuint clothes_color_buffer = glGetProgramResourceIndex(
      clothes_prog, GL_SHADER_STORAGE_BLOCK, "ColorBuffer");
  glShaderStorageBlockBinding(clothes_prog, clothes_color_buffer, 1);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, clothes_vertex_color);
  GL::checkError();

  GLuint vertex_buffer = glGetProgramResourceIndex(
      clothes_prog, GL_SHADER_STORAGE_BLOCK, "VertexBuffer");
  glShaderStorageBlockBinding(clothes_prog, vertex_buffer, 2);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, clothes_vertex_buffer[0]);
  GL::checkError();

  GLint gridsize = glGetUniformLocation(clothes_prog, "gridSize");
  glUniform2i(gridsize, gridSize, gridSize);
  GL::checkError();

  initGrid();

  sim.registerBuffers(clothes_vertex_buffer);

  switchScene(0);
}

void Renderer::initGrid()
{
  currentVertices = 0;

  std::vector<math::float4> initPositions;
  initPositions.reserve(gridSize * gridSize);
  std::vector<math::float4> colors;
  colors.reserve(gridSize * gridSize);
  float xstep = cloth_size_x / (gridSize - 1);
  float zstep = cloth_size_z / (gridSize - 1);
  float inv_gridSize = 1.0f / (gridSize - 1);
  for (size_t j = 0; j < gridSize; ++j)
    for (size_t i = 0; i < gridSize; ++i)
    {
      initPositions.emplace_back(
          math::float4(-0.5f * cloth_size_x + i * xstep, cloth_height,
                       -0.5f * cloth_size_z + j * zstep, 1));
      colors.emplace_back(
          math::float4(i * inv_gridSize, j * inv_gridSize, 0.5f, 1.0f));
    }

  // set positions for all buffers the same
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, clothes_vertex_color);
  glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, gridSize * gridSize * 4 * 4,
                  &colors[0]);
  for (int i = 0; i < 4; ++i)
  {
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, clothes_vertex_buffer[i]);
    glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, gridSize * gridSize * 4 * 4,
                    &initPositions[0]);
  }
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void Renderer::updateSimulation(double now)
{
  sim.mapBuffers();
  int numSimulation;
  if (lastSimulation < 0)
  {
    lastSimulation = now;
    numSimulation = 1;
  }
  else
  {
    numSimulation = static_cast<int>((now - lastSimulation) /
                                     (double)simulationStep * simulationSpeed);
    lastSimulation += numSimulation * simulationStep;
  }

  if (numSimulation > maxSimulationsPerFrame)
  {
    numSimulation = maxSimulationsPerFrame;
  }

  if (benchmark)
  {
    numSimulation = 1;
  }

  for (int i = 0; i < numSimulation; ++i)
  {
    if (currentWind == WindType::MovingSide)
    {
      static std::random_device rd;
      static std::mt19937 gen(rd());
      float perParticleFactor = cloth_mass / (128 * gridSize);
      std::uniform_real_distribution<float> dist(-20.0f * perParticleFactor,
                                                 20.0f * perParticleFactor);
      wind_x += dist(gen);
      wind_z += dist(gen);
      float maxwind = 5000.0f * perParticleFactor;
      wind_x = std::min(maxwind, std::max(-maxwind, wind_x));
      wind_z = std::min(maxwind, std::max(-maxwind, wind_z));
    }
    cudaEventRecord(start);
    sim.simulate(simulationStep, wind_x, wind_y, wind_z, currentVertices);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float ms;
    cudaEventElapsedTime(&ms, start, stop);
    simulationTime = ms / 1000.0f;
  }
  sim.unmapBuffers();
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::adjustSimulationSpeed(double factor)
{
  simulationSpeed *= factor;
}

void Renderer::switchRendermode()
{
  renderMode = (renderMode + 1) % 2;
}

void Renderer::switchScene(int i)
{
  initGrid();
  std::vector<std::tuple<int, int>> fixedNodes;
  obstacles.clear();

  currentScene = i;

  switch (i)
  {
  default:
  case 0:
    std::cout << "Scene 0: 2 fixed vertices\n";
    fixedNodes.push_back(std::make_tuple(0, 0));
    fixedNodes.push_back(std::make_tuple(gridSize - 1, 0));
    break;
  case 1:
    std::cout << "Scene 1: 1 fixed vertices\n";
    fixedNodes.push_back(std::make_tuple(0, 0));
    break;
  case 2:
    std::cout << "Scene 2: 3 fixed vertices\n";
    fixedNodes.push_back(std::make_tuple(0, 0));
    fixedNodes.push_back(std::make_tuple(gridSize - 1, 0));
    fixedNodes.push_back(std::make_tuple(gridSize - 1, gridSize - 1));
    break;
  case 3:
    std::cout << "Scene 3: 2 fixed vertices and an obstacle\n";
    fixedNodes.push_back(std::make_tuple(0, 0));
    fixedNodes.push_back(std::make_tuple(gridSize - 1, 0));
    obstacles.push_back(math::float4(0, 0.5f, -0.1f, 0.2f));
    break;
  case 4:
    std::cout << "Scene 4: 2 fixed vertices and 2 obstacles\n";
    fixedNodes.push_back(std::make_tuple(0, 0));
    fixedNodes.push_back(std::make_tuple(gridSize - 1, 0));
    obstacles.push_back(math::float4(-0.3f, 0.5f, -0.1f, 0.2f));
    obstacles.push_back(math::float4(0.3f, 0.5f, -0.1f, 0.2f));
    break;
  case 5:
    std::cout << "Scene 5: Free falling and an obstacle\n";
    obstacles.push_back(math::float4(0, 0, 0, 0.5f));
    break;
  case 6:
    std::cout << "Scene 6: Free falling and 4 obstacles\n";
    obstacles.push_back(math::float4(-0.3f, 0, -0.3f, 0.25f));
    obstacles.push_back(math::float4(-0.3f, 0, 0.3f, 0.25f));
    obstacles.push_back(math::float4(0.3f, 0, -0.3f, 0.25f));
    obstacles.push_back(math::float4(0.3f, 0, 0.3f, 0.25f));
    break;
  case 7:
    std::cout << "Scene 7: Free falling and 3 obstaclse\n";
    obstacles.push_back(math::float4(-0.3f, 0.2f, -0.3f, 0.25f));
    obstacles.push_back(math::float4(0.2f, -0.3f, 0.4f, 0.25f));
    obstacles.push_back(math::float4(0.4f, -0.8f, -0.1f, 0.25f));
    break;
  }

  sim.removeObstacles();
  for (auto& obstacle : obstacles)
    sim.addObstacles(obstacle.x, obstacle.y, obstacle.z, obstacle.w);
  sim.newCloth(gridSize, cloth_size_x, cloth_size_z, cloth_mass,
               fixupIterations, fixupPercent,
               fixedNodes.size() > 0 ? &fixedNodes[0] : nullptr,
               fixedNodes.size());
}

void Renderer::toggleWind()
{
  float perParticleFactor = cloth_mass / (128 * gridSize);
  switch (currentWind)
  {
  case WindType::NoWind:
    currentWind = WindType::LowSide;
    std::cout << "low side wind\n";
    wind_y = 0;
    wind_x = 25.0f * perParticleFactor;
    wind_z = 1500.0f * perParticleFactor;
    break;
  case WindType::LowSide:
    currentWind = WindType::StrongSide;
    std::cout << "strong side wind\n";
    wind_y = 0;
    wind_x = 70.0f * perParticleFactor;
    wind_z = 2800.0f * perParticleFactor;
    break;
  case WindType::StrongSide:
    currentWind = WindType::LowBottom;
    std::cout << "low bottom wind\n";
    wind_y = 1600.0f * perParticleFactor;
    wind_x = 0;
    wind_z = 0;
    break;
  case WindType::LowBottom:
    currentWind = WindType::StrongBottom;
    std::cout << "strong bottom wind\n";
    wind_y = 3600.0f * perParticleFactor;
    wind_x = 0;
    wind_z = 0;
    break;
  case WindType::StrongBottom:
    currentWind = WindType::MovingSide;
    std::cout << "moving side wind\n";
    wind_y = 0;
    wind_x = 20.0f * perParticleFactor;
    wind_z = 2800.0f * perParticleFactor;
    break;
  case WindType::MovingSide:
    currentWind = WindType::NoWind;
    std::cout << "no wind\n";
    wind_x = wind_y = wind_z = 0;
    break;
  }
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::render()
{
  double now = std::chrono::duration_cast<std::chrono::duration<double>>(
                   std::chrono::high_resolution_clock::now() - refTime)
                   .count();
  if (!frozen)
  {
    updateSimulation(now);
  }

  Camera::UniformBuffer cam_params;
  camera.writeUniformBuffer(
      &cam_params, navigator,
      static_cast<float>(viewport_width) / viewport_height);

  GL::checkError();
  glBindBuffer(GL_UNIFORM_BUFFER, camera_uniform_buffer);
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(Camera::UniformBuffer),
                  &cam_params);
  glBindBufferRange(GL_UNIFORM_BUFFER, 0, camera_uniform_buffer, 0,
                    sizeof(Camera::UniformBuffer));

  GL::checkError();
  glViewport(0, 0, viewport_width, viewport_height);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  drawClothes();
  drawObstacles();
  frame_counter++;

  if (now - lastDisplayUpdate > 1.0)
  {
    lastDisplayUpdate = now;
    std::stringstream info;
    if (!frozen)
    {
      info << "sim: " << std::setprecision(5) << (simulationTime * 1000)
           << "ms  ";
      info << "sim-speed: " << std::setprecision(3) << simulationSpeed << "x  ";
    }
    else
    {
      info << "frozen  ";
    }
    info << "scene: " << currentScene << "  ";
    info << deftitle;
    window.title(info.str().c_str());
  }

  GL::checkError();
  swapBuffers();
}

///////////////////////////////////////////////////////////////////////////////

void Renderer::close()
{
  GL::platform::quit();
}

///////////////////////////////////////////////////////////////////////////

void Renderer::destroy()
{
  GL::platform::quit();
}

///////////////////////////////////////////////////////////////////////////

void Renderer::move(int x, int y)
{
}

///////////////////////////////////////////////////////////////////////////////
