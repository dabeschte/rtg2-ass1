

#version 430

layout(location = 2) uniform ivec2 gridSize;

out ivec2 grid_id;

void main()
{
  grid_id = ivec2(gl_VertexID%(gridSize.x-1), gl_VertexID / (gridSize.x-1));
}
