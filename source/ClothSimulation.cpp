#include "ClothSimulation.h"
#include <cuda_gl_interop.h>
#include <framework/cuda/CheckError.h>

////
#include <vector>
////

ClothSimulation::ClothSimulation(int gridDim, float sizex, float sizez,
                                 float cloth_mass, float damping, float gravity,
                                 int fixUpIterations, float fixUpPercent,
                                 const std::tuple<int, int>* fixedNodes,
                                 size_t numFixed)
  : damping(damping)
  , gravity(gravity)
{
  for (int i = 0; i < BUFFERCOUNT; ++i)
    positions_buffers[i] = nullptr;
  newCloth(gridDim, sizex, sizez, cloth_mass, fixUpIterations, fixUpPercent,
           fixedNodes, numFixed);
}

ClothSimulation::~ClothSimulation()
{
  for (int i = 0; i < BUFFERCOUNT; ++i)
    if (positions_buffers[i] != nullptr)
      checkCudaError(cudaGraphicsUnregisterResource(positions_buffers[i]));

}

void ClothSimulation::mapBuffers()
{
  checkCudaError(cudaGraphicsMapResources(BUFFERCOUNT, positions_buffers));

  size_t num_bytes;
  for (int i = 0; i < BUFFERCOUNT; ++i)
    checkCudaError(cudaGraphicsResourceGetMappedPointer(
        reinterpret_cast<void**>(&mapped_positions[i]), &num_bytes,
        positions_buffers[i]));
}

void ClothSimulation::unmapBuffers()
{
  checkCudaError(cudaGraphicsUnmapResources(BUFFERCOUNT, positions_buffers));
  //prePreviousPositionBuffer = -1;
}

// why the heck do you create 4 gl buffers but only use 3 here? ;)
void ClothSimulation::registerBuffers(GL::Buffer buffers[BUFFERCOUNT])
{
  for (int i = 0; i < BUFFERCOUNT; ++i)
    if (positions_buffers[i] != nullptr)
      checkCudaError(cudaGraphicsUnregisterResource(positions_buffers[i]));
  for (int i = 0; i < BUFFERCOUNT; ++i)
    checkCudaError(cudaGraphicsGLRegisterBuffer(
        &positions_buffers[i], buffers[i], cudaGraphicsMapFlagsNone));
}
