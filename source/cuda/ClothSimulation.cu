#include "../ClothSimulation.h"
#include <framework/cuda/CheckError.h>
#include <framework/cuda/helper_math.h>
#include "device_launch_parameters.h"

__device__ float3 normalizedNormal(float4 p1, float4 p2, float4 p3)
{
  float3 n;
  float4 v = p2 - p1;
  float4 w = p3 - p1;

  n.x = (v.y * w.z) - (v.z * w.y);
  n.y = (v.z * w.z) - (v.x * w.z);
  n.z = (v.x * w.y) - (v.y * w.x);

  return normalize(n);
}

__device__ void addWindForce(const float4* __restrict p_in, int num_particles, 
  int x1, int y1, int x2, int y2, float4 basePos, float4* force, float3 wind, float mass)
{
  if(x1 < 0 || x2 < 0 || y1 < 0 || y2 < 0 
    || x1 >= num_particles || x2 >= num_particles || y1 >= num_particles || y2 >= num_particles)
  {
    return;
  }

  int particle1 = y1 * num_particles + x1;
  int particle2 = y2 * num_particles + x2;
  float4 pos1 = p_in[particle1];
  float4 pos2 = p_in[particle2];

  float3 normal = normalizedNormal(basePos, pos2, pos1);
  float factor = dot(normal, -1.0f * normalize(wind)) / mass;
  if(factor > 0)
  {
    float4 wind4 = make_float4(wind.x, wind.y, wind.z, 0.0f);
    *force += wind4 * factor;
  }
}
// example kernel to start with
__global__ void simulation_step(int num_particles, const float4* __restrict p_previous,
                                const float4* __restrict p_in, float4* p_out, float dt,
                                float mass, int* dev_fixedPos, int numFixed,
                                float windX, float windY, float windZ)
{
  // compute thread indices
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;
  if (x >= num_particles || y >= num_particles)
    return;

  // return immediately if this node is fixed
  if (numFixed > 0)
  {
    for (int i = 0; i < numFixed; i++)
    {
      if (dev_fixedPos[i * 2] == x && dev_fixedPos[i * 2 + 1] == y)
        return;
    }
  }

  // compute particle index
  int myParticle = y * num_particles + x;
  float4 myPos = p_in[myParticle];
  float4 myPreviousPos = p_previous[myParticle];
  float3 wind = make_float3(windX, windY, windZ);

  float4 force = make_float4(0.0f, -9.81f, 0.0f, 0.0f);
  const float damp = 0.995f;

  // i have no idea why this works
  float factor = mass / dt * .35f;

  // add wind
  addWindForce(p_in, num_particles, x - 1, y, x, y + 1, myPos, &force, wind, factor);
  addWindForce(p_in, num_particles, x, y + 1, x + 1, y, myPos, &force, wind, factor);
  addWindForce(p_in, num_particles, x + 1, y, x, y - 1, myPos, &force, wind, factor);
  addWindForce(p_in, num_particles, x, y - 1, x - 1, y, myPos, &force, wind, factor);

  // move the particle (this is a verlet integration!)
  p_out[myParticle] = (1.0f + damp) * myPos - damp * myPreviousPos + force * dt * dt;
}

__device__ void addSpringForce(const float4* __restrict p_in, int num_particles, int x, int y, float baseDistance, float4 targetPos, float4* force)
{
  if(x < 0 || y < 0 || x >= num_particles || y >= num_particles)
  {
    return;
  }

  int myParticle = y * num_particles + x;
  float4 myPos = p_in[myParticle];

  float4 posDiff = targetPos - myPos;
  float distance = sqrtf(dot(posDiff, posDiff));
  float distanceDiff = (baseDistance - distance) * 0.5f;

  if(distance != 0)
  {
    *force += (posDiff / distance) * distanceDiff;
  }
}

__device__ float distance(float3 p1, float3 p2)
{
  float3 posDiff = p2 - p1;
  return sqrtf(abs(dot(posDiff, posDiff)));
}

__global__ void fixUp(int num_particles, const float4* __restrict p_old,
  const float4* __restrict p_in, float4* p_out,
  int* dev_fixedPos, int numFixed, float fixUpPercent, 
  const float3* __restrict dev_obstaclePos, const float* __restrict dev_obstacleRadius, const float numObstacles,
  float mass, float baseDistance, float doubleBaseDistance, float diagonalDistance)
{
  // compute thread indices
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;
  if (x >= num_particles || y >= num_particles)
    return;

  // return immediately if this node is fixed
  if (numFixed > 0)
  {
    for (int i = 0; i < numFixed; i++)
    {
      if (dev_fixedPos[i * 2] == x && dev_fixedPos[i * 2 + 1] == y)
        return;
    }
  }

  // compute particle index
  int myParticle = y * num_particles + x;
  float4 myPos = p_in[myParticle];
  float4 force = make_float4(0);

  // structural springs
  addSpringForce(p_in, num_particles, x - 1, y, baseDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x + 1, y, baseDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x, y - 1, baseDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x, y + 1, baseDistance, myPos, &force);

  // structural springs
  addSpringForce(p_in, num_particles, x - 1, y - 1, diagonalDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x + 1, y - 1, diagonalDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x - 1, y + 1, diagonalDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x + 1, y + 1, diagonalDistance, myPos, &force);

  // flexion springs
  addSpringForce(p_in, num_particles, x + 2, y, doubleBaseDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x - 2, y, doubleBaseDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x, y + 2, doubleBaseDistance, myPos, &force);
  addSpringForce(p_in, num_particles, x, y - 2, doubleBaseDistance, myPos, &force);

  float4 myNewPos = myPos + force * fixUpPercent;

  // check collisions
  float3 myPos3 = make_float3(myNewPos.x / myNewPos.w, myNewPos.y / myNewPos.w, myNewPos.z / myNewPos.w);
  for(int i = 0; i < numObstacles; i++)
  {
    float3 o = dev_obstaclePos[i];
    float r = dev_obstacleRadius[i];
    float d = distance(myPos3, o);
    
    // check if inside obstacle
    if(d < r)
    {
      // push outside
      float3 v = normalize(myPos3 - o);
      myNewPos = make_float4(
        o.x + v.x * r,
        o.y + v.y * r, 
        o.z + v.z * r, 
        1.0f);
      break;
    }
  }

  p_out[myParticle] = myNewPos;
}


void ClothSimulation::simulate(float dt, float windx, float windy, float windz,
                               int& previousPositionBuffer)
{
  // TODO:
  // simulate the cloth particles using CUDA
  // dt it the timestep of the simulation, it is constant throughout one run
  // (windx, windy, windz) describe the windforce applied to a particle
  // lastPosition is the index of the buffer that was last used as position
  //	buffer and is to be set by the routine to tell the renderer from which buffer to
  //	render

  // perform verlet integration for the particle grid
  // 1. launch a CUDA kernel to perform the simulation step
  // 2. launch a CUDA kernel fixUpIterations times to perform the verlet fix up
  // step, use
  //    fixUpPercent for the amount of fixup to apply for the spring forces of
  //    the cloth,
  //    e.g. 0.3 -> move the particles 30% of the distance of a full fix up
  //    for the spherical obstacles perform a full fixup after applying the fixup
  //    for the
  //    spring forces, i.e. move completely out of the hit sphere along the
  //    sphere normal

  // variables you will need:
  // gravity the acceleration to be applied along the positive y direction of
  //	each particle (i.e. gravity is negative)
  // damping the velocity dampening factor (e.g. 0.995 = use99.5% of the
  //	velocity to perform the position update)
  // windx, windy, windz, corresponds to the wind force that should be applied
  //	to every particle whose normal is parallel
  //	to the wind, i.e., compute the dot product of the estimated normal of the
  //	particle with the wind to compute the wind
  //	force on the particle
  // gridDim the number of particles along one dimension (i.e. you have to
  //	simulate gridDim * gridDim particles)
  // mapped_positions are pointers to three OpenGL pointers each storing the
  //	particle positions as float4
  // previousPositionBuffer is the index of the last buffer that was used for
  //   rendering, i.e. the current position
  // for verlet integration you will also need the previous positions, for the
  //	fixup you might need another buffer
  //	to toggle between input and output buffers. Make sure you do not destroy
  //	the last positions as you need
  //	them for the verlet integration. Also update previousPositionBuffer so that
  //	the renderer knows which buffer contains
  //	the current positions. The framework will not tough the buffers, unless
  //	the simulation is reset.

  //  simple example
  if(prePreviousPositionBuffer == -1)
  {
    prePreviousPositionBuffer = previousPositionBuffer;
  }

  int nextPosbuffer = (previousPositionBuffer + 1) % BUFFERCOUNT;
  dim3 block(16, 16);
  dim3 threadGrid((gridDim + block.x - 1) / block.x,
                  (gridDim + block.y - 1) / block.y);

  simulation_step<<<threadGrid, block>>>(gridDim,
    mapped_positions[prePreviousPositionBuffer],
    mapped_positions[previousPositionBuffer],
    mapped_positions[nextPosbuffer], 
    dt, particle_mass,
    dev_fixedPos, numFixed, windz, windy, windx);
  
  cudaError_t cudaError = cudaGetLastError();
  if (cudaError)
  {
    printf("CUDA ERROR in fixup: code:0x%x  name:%s  text:%s",
      cudaError,
      cudaGetErrorName(cudaError),
      cudaGetErrorString(cudaError));
    exit(-1);
  }

  float baseDistance = sizex / gridDim;
  float doubleBaseDistance = baseDistance * 2.0f;
  float diagonalDistance = sqrtf(2.0f * baseDistance * baseDistance);
  prePreviousPositionBuffer = previousPositionBuffer;
  for (int i = 0; i < fixUpIterations; i++)
  { 
    previousPositionBuffer = nextPosbuffer;
    if(i % 2 == 1)
    {
      nextPosbuffer = (nextPosbuffer - 1 + BUFFERCOUNT) % BUFFERCOUNT;
    } else
    {
      nextPosbuffer = (nextPosbuffer + 1 + BUFFERCOUNT) % BUFFERCOUNT;
    }


    fixUp << <threadGrid, block >> >(gridDim,
      mapped_positions[(prePreviousPositionBuffer - 1 + BUFFERCOUNT) % 4],
      mapped_positions[previousPositionBuffer],
      mapped_positions[nextPosbuffer],
      dev_fixedPos, numFixed, fixUpPercent, 
      dev_obstaclePos, dev_obstacleRadius, numObstacles,
      particle_mass, baseDistance, doubleBaseDistance, diagonalDistance
      );

    cudaError_t cudaError = cudaGetLastError();
    if (cudaError)
    {
      printf("CUDA ERROR in fixup: code:0x%x  name:%s  text:%s",
        cudaError,
        cudaGetErrorName(cudaError),
        cudaGetErrorString(cudaError));
      exit(-1);
    }
  }

  previousPositionBuffer = nextPosbuffer;
}

void ClothSimulation::newCloth(int gridDim, float sizex, float sizez,
                               float cloth_mass, int fixUpIterations,
                               float fixUpPercent,
                               const std::tuple<int, int>* fixedNodes,
                               size_t numFixed)
{
  this->gridDim = gridDim;
  this->sizex = sizex;
  this->sizez = sizez;
  this->particle_mass = cloth_mass / (gridDim * gridDim);
  this->fixUpIterations = fixUpIterations;
  this->fixUpPercent = fixUpPercent;
  this->numFixed = numFixed;

  prePreviousPositionBuffer = -1;

  if (numFixed > 0)
  {
    checkCudaError(cudaMalloc(&dev_fixedPos, sizeof(int) * 2 * numFixed));
    checkCudaError(cudaMemcpy(dev_fixedPos, fixedNodes, sizeof(int) * 2 * numFixed, cudaMemcpyHostToDevice));
  }
}

void ClothSimulation::addObstacles(float x, float y, float z, float radius)
{
  numObstacles++;
  float3 obstaclePos = make_float3(x, y, z);
  float3* newObstaclePos;
  float* newObstacleRadius;
  checkCudaError(cudaMalloc(&newObstaclePos, sizeof(float) * 3 * numObstacles));
  checkCudaError(cudaMalloc(&newObstacleRadius, sizeof(float) * numObstacles));
  if(numObstacles > 1 && dev_obstacleRadius != nullptr && dev_obstaclePos != nullptr)
  {
    checkCudaError(cudaMemcpy(newObstaclePos, dev_obstaclePos, sizeof(float) * 3 * (numObstacles - 1), cudaMemcpyDeviceToDevice));
    checkCudaError(cudaMemcpy(newObstacleRadius, dev_obstacleRadius, sizeof(float) * (numObstacles - 1), cudaMemcpyDeviceToDevice));
  }
  checkCudaError(cudaMemcpy(newObstaclePos + (numObstacles - 1), &obstaclePos, sizeof(float) * 3, cudaMemcpyHostToDevice));
  checkCudaError(cudaMemcpy(newObstacleRadius + (numObstacles - 1), &radius, sizeof(float), cudaMemcpyHostToDevice));

  dev_obstaclePos = newObstaclePos;
  dev_obstacleRadius = newObstacleRadius;
}

void ClothSimulation::removeObstacles()
{
  if(dev_obstaclePos != nullptr)
  {
    checkCudaError(cudaFree(dev_obstaclePos));
    checkCudaError(cudaFree(dev_obstacleRadius));
  }

  dev_obstaclePos = nullptr;
  dev_obstacleRadius = nullptr;
  numObstacles = 0;
}
