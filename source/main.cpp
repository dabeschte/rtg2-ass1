#include "InputHandler.h"
#include "Renderer.h"

#include "framework/argparse.h"
#include "framework/cuda/CheckError.h"
#include "framework/math/math.h"
#include "framework/utils/OrbitalNavigator.h"
#include "framework/utils/PerspectiveCamera.h"

#include "GL/platform/Application.h"
#include "GL/platform/Window.h"

#include <cstring>
#include <iostream>
#include <sstream>
#include <stdexcept>

struct invalid_device : std::runtime_error
{
  explicit invalid_device(const std::string& msg)
    : runtime_error(msg)
  {
  }
};

void printUsage(const char* argv0)
{
  std::cout << "usage: " << argv0
            << " [options]\n"
               "options:\n"
               "  --device N     (use cuda device with index N)\n"
               "  --frozen       (start with simulation frozen)\n"
               "  --gridsize N   (use an NxN particle grid)\n"
               "  --size N       (set the size of the cloth to NxN)\n"
               "  --height N     (starting height of the cloth)\n"
               "  --mass N       (mass of the cloth)\n"
               "  --steps N      (fix up steps in each iteration)\n"
               "  --fixratio N   (fix up step fix ratio)\n";
}

int main(int argc, char* argv[])
{
  try
  {
    int cudaDevice = 0;
    int grid_size = 256;
    bool start_frozen = false;
    float cloth_dim = 2.0f;
    float height = 1.5f;
    float mass = 0.05f;
    int steps = 30;
    float fixup_perc = 0.3f;

    const char frozen_token[] = "--frozen";
    const char device_token[] = "--device";
    const char grid_size_token[] = "--gridsize";
    const char cloth_dim_token[] = "--size";
    const char height_token[] = "--height";
    const char mass_token[] = "--mass";
    const char steps_token[] = "--steps";
    const char fixup_token[] = "--fixratio";

    for (char** a = &argv[1]; *a; ++a)
    {
      if (std::strcmp("--help", *a) == 0)
      {
        printUsage(argv[0]);
        return 0;
      }
      if (!argparse::checkArgument(frozen_token, a, start_frozen))
        if (!argparse::checkArgument(device_token, a, cudaDevice))
          if (!argparse::checkArgument(grid_size_token, a, grid_size))
            if (!argparse::checkArgument(cloth_dim_token, a, cloth_dim))
              if (!argparse::checkArgument(height_token, a, height))
                if (!argparse::checkArgument(mass_token, a, mass))
                  if (!argparse::checkArgument(steps_token, a, steps))
                    if (!argparse::checkArgument(fixup_token, a, fixup_perc))
                      std::cout << "warning: unknown option " << *a
                                << " will be ignored" << std::endl;
    }

    if (start_frozen)
    {
      std::cout << "starting with frozen simulation" << std::endl;
    }

    int numCudaDevices = 0;

    checkCudaError(cudaGetDeviceCount(&numCudaDevices));

    if (cudaDevice < 0 || cudaDevice >= numCudaDevices)
    {
      std::ostringstream msg;
      msg << "specified cuda device index (" << cudaDevice << ") is not valid!"
          << std::endl
          << "       available cuda devices: " << numCudaDevices << std::endl;
      throw invalid_device(msg.str());
    }

    cudaDeviceProp props;
    cudaGetDeviceProperties(&props, cudaDevice);
    std::cout << "using cuda device " << cudaDevice << ":" << std::endl;
    std::cout << "    " << props.name << std::endl;
    std::cout << "    with cc " << props.major << "." << props.minor << " @"
              << props.clockRate / 1000 << "MHz" << std::endl;

    checkCudaError(cudaSetDevice(cudaDevice));

    std::cout << "Simulation setup: \n  " << grid_size << "x" << grid_size
              << " simulation grid\n  " << cloth_dim << "x" << cloth_dim
              << " cloth at " << height << " height with a mass of " << mass
              << "\n  " << steps << " fixup iterations with a fixup of "
              << fixup_perc << " each\n";

    GL::platform::Window window("Assignment 1", 1024, 768, 0, 0, false);

    PerspectiveCamera camera;

    OrbitalNavigator navigator(-0.75f * math::constants<float>::pi(),
                               0.22f * math::constants<float>::pi(), 4.2f);

    Renderer renderer(window, camera, navigator, 4, 4, grid_size, cloth_dim,
                      height, mass, steps, fixup_perc);

    if (start_frozen)
    {
      renderer.freeze();
    }

    InputHandler input_handler(navigator, renderer);

    window.attach(
        static_cast<GL::platform::KeyboardInputHandler*>(&input_handler));
    window.attach(
        static_cast<GL::platform::MouseInputHandler*>(&input_handler));
    window.attach(static_cast<GL::platform::DisplayHandler*>(&renderer));
    window.attach(static_cast<GL::platform::MouseInputHandler*>(&navigator));

    GL::platform::run(renderer);
  }
  catch (const argparse::usage_error& e)
  {
    std::cout << "error: " << e.what() << std::endl;
    printUsage(argv[0]);
    return 1;
  }
  catch (const std::exception& e)
  {
    std::cout << "error: " << e.what() << std::endl;
    return -1;
  }
  catch (...)
  {
    std::cout << "unknown exception" << std::endl;
    return -128;
  }

  return 0;
}
