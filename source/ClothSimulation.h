#ifndef CLOTHSIMULATION_H_POW5AKL9
#define CLOTHSIMULATION_H_POW5AKL9

#pragma once

#include "GL/buffer.h"
#include <cuda_runtime.h>
#include <tuple>
#include <thrust\host_vector.h>

#define BUFFERCOUNT 4

class ClothSimulation
{
  int gridDim;
  int fixUpIterations;
  float sizex, sizez;
  float particle_mass;
  float fixUpPercent;
  float damping;
  float gravity;
  int prePreviousPositionBuffer = -1;


  cudaGraphicsResource_t positions_buffers[BUFFERCOUNT];
  float4* mapped_positions[BUFFERCOUNT];

// TODO: feel free to add any member you need here

  int* dev_fixedPos = NULL;
  float3* dev_obstaclePos = NULL;
  float* dev_obstacleRadius = NULL;
  size_t numObstacles = 0;
  size_t numFixed = 0;

public:
  ClothSimulation(int gridDim, float sizex, float sizez, float cloth_mass,
                  float damping, float gravity, int fixUpIterations,
                  float fixUpPercent, const std::tuple<int, int>* fixedNodes,
                  size_t numFixed);
  ~ClothSimulation();
  void mapBuffers();
  void unmapBuffers();
  void simulate(float dt, float windx, float windy, float windz,
                int& lastPositionBuffer);
  void newCloth(int gridDim, float sizex, float sizez, float cloth_mass,
                int fixUpIterations, float fixUpPercent,
                const std::tuple<int, int>* fixedNodes, size_t numFixed);
  void registerBuffers(GL::Buffer buffers[3]);
  void removeObstacles();
  void addObstacles(float x, float y, float z, float radius);
};

#endif 